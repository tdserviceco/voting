$('.vote-for-me').on('click', function (e) {
    e.preventDefault();
    if (checkCookie() !== false) {
        var button_id = $(this);
        var post_id = button_id.attr('data-post-id');
        var votes = button_id.find('.badge').attr('data-post-id');
        var data = {
            action: 'my_action',
            post_id: post_id,
            votes: votes,
        };

        var ajaxurl = ajax_function.ajax_url;

        $.post(ajaxurl, data)
            .done(function (response) {
                //console.log(response);
                location.reload();
            })
            .fail(function (response) {
                console.log(response);
            });
    }

});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}


function checkCookie() {
    var vote = getCookie('vote');
    if (vote != "") {
        alert(translation.already_voted);
        return false;
    } else {
        setCookie("vote", "voted", 30);
        alert(translation.thank_you);
    }
}

