module.exports = function ( grunt ) {

    // load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
    require('load-grunt-tasks')(grunt);
    require( 'time-grunt' )( grunt );

    // Project configuration.
    grunt.initConfig( {
        pkg: grunt.file.readJSON( 'package.json' ),
        sass: {
            options: {
                sourceMap: true,
                includePaths: [ 'bower_components/' ]
            },
            dev: {
                files: {
                    'assets/css/app.css': 'assets/css/app.scss'
                }
            },
            dist: {
                options: {
                    sourceMap: false,
                    outputStyle: 'compressed'
                },
                files: {
                    'assets/css/app.css': 'assets/css/app.scss'
                }
            }
        },
        import: {
            options: {},
            dev: {
                src: 'assets/js/dev/app.js',
                dest: 'assets/js/app.js'
            }
        },
        watch: {
            options: {
                livereload: true
            },
            styles: {
                files: ['assets/css/**/*.scss'],
                tasks: [ 'sass:dev' ],
                options: {
                    interrupt: true
                }
            },
            js: {
                files: ['assets/js/dev/**/*.js'],
                tasks: [ 'import:dev' ],
                options: {
                    interrupt: true
                }
            }
        }
    } );

    // Default task(s).
    grunt.registerTask( 'default', [ 'sass:dev', 'import', 'watch' ] );
    grunt.registerTask( 'dist', [ 'sass:dist', 'import' ] );

};