<?php
/*
Plugin Name: Voting WP
Description: Voting plugin simple and saves votes and using cookies
Version:     1.0
Author:      iqq
Author URI:  http://iqq.se
Text Domain: voting
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class translation {
	static function text_domain_translation() {
		add_action( 'init', array( __CLASS__, 'voting_load_textdomain' ) );
	}

	static function voting_load_textdomain() {
		load_plugin_textdomain( 'voting', false, basename( dirname( __FILE__ ) ) . '/languages' );
	}
}

class VOTING extends translation {

	static function init() {
		parent::text_domain_translation();
		add_action( 'init', array( __CLASS__, 'voting_load_textdomain' ) );
		add_action( 'init', array( __CLASS__, 'create_posttype' ) );
		add_filter( 'manage_edit-vote_columns', array( __CLASS__, 'my_columns' ) );
		add_action( 'manage_posts_custom_column', array( __CLASS__, 'my_show_columns' ) );

	}

	static function my_columns( $columns ) {

		unset( $columns['date'] );
		$columns['votes'] = __( 'Votes', 'voting' );

		return $columns;
	}

	static function my_show_columns( $name ) {
		global $post;
		$post_meta = get_post_meta( get_the_ID(), 'vote', true );
		switch ( $name ) {
			case 'votes':
				if ( empty( $post_meta ) ) {
					echo "0";
				} else {
					echo $post_meta;
				}
		}
	}

	static function create_posttype() {

		$support = array(
			'title',
			'thumbnail',
			'editor',
//            'revisions',
			'excerpt'
		);

		$taxonomies = array(
			'category',
//            'post_tag'
		);

		$labels = array(
			'name'               => __( 'Vote', 'voting' ),
			'singular_name'      => __( 'Vote', 'voting' ),
			'add_new'            => __( 'Add Attendant', 'voting' ),
			'all_items'          => __( 'All Attendee', 'voting' ),
			'add new items'      => __( 'Add Attendant', 'voting' ),
			'edit_item'          => __( 'Edit Attendee', 'voting' ),
			'view_item'          => __( 'View Attendant', 'voting' ),
			'search_item'        => __( 'Search Attendant', 'voting' ),
			'not_found'          => __( 'No Attendant found', 'voting' ),
			'not_found_in_trash' => __( 'No Attendant found in trash', 'voting' ),
			'parent_item_colon'  => __( 'Parent Attendant', 'voting' )
		);
		$args   = array(
			'labels'              => $labels,
			'public'              => true,
			'has_archive'         => true,
			'publicly_queryable'  => true,
			'query_var'           => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => $support,
			'taxonomies'          => $taxonomies,
			'meny_position'       => 5,
			'menu_icon'           => 'dashicons-thumbs-up',
			'exclude_from_search' => true,
		);

		register_post_type( 'vote', $args );

	}
}

VOTING::init();

CLASS VOTING_shortcode extends translation {

	static function init() {
		parent::text_domain_translation();
		add_shortcode( 'vote', array( __CLASS__, 'vote_button' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'fontawesome' ) );
		add_action( 'wp_ajax_my_action', array( __CLASS__, 'my_action_callback' ) );
		add_action( 'wp_ajax_nopriv_my_action', array( __CLASS__, 'my_action_callback' ) );

	}


	static function my_action_callback() {
		echo __( "Thanks for voting!", 'voting' );
		$ajax_response = $_POST['post_id'];
		if ( isset( $ajax_response ) ) {
			$check_meta = get_post_meta( $ajax_response, 'vote' );
			if ( empty( $check_meta ) ) {
				add_post_meta( $ajax_response, 'vote', '1' );
			} else {
				$votes = $_POST['votes'];
				$votes ++;
				update_post_meta( $ajax_response, 'vote', $votes );
			}

		}
		wp_die();
	}

	static function fontawesome() {
		wp_enqueue_script( 'vote-plugin', plugin_dir_url( __FILE__ ) . 'assets/js/app.js', array( 'jquery' ), false, true );
		wp_enqueue_style( 'plugin-stylesheet', plugin_dir_url( __FILE__ ) . 'assets/css/app.css' );


		$translation = array(
			'thank_you'     => __( 'Thank you for voting', 'voting' ),
			'already_voted' => __( 'Sorry you already voted!', 'voting' ),
		);


		$ajax = array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		);

		wp_localize_script( 'vote-plugin', 'ajax_function', $ajax );
		wp_localize_script( 'vote-plugin', 'translation', $translation );

	}


	static function checkIfMetaEmpty() {
		$post_meta = get_post_meta( get_the_ID(), 'vote', true );
		if ( empty( $post_meta ) ) {
			echo "<button class='btn btn-default vote-for-me' data-post-id='" . get_the_ID() . "'>" . __( 'Vote', 'voting' ) . "<span class='badge' data-post-id='0'>0</span>";
		} else {
			echo "<button class='btn btn-default vote-for-me' data-post-id='" . get_the_ID() . "'>" . __( 'Vote', 'voting' ) . "<span class='badge' data-post-id='" . $post_meta . "'>" . $post_meta . "</span>";
		}
	}

	static function vote_button( $atts, $content = null ) {


		$shortcode = VOTING_shortcode::checkIfMetaEmpty();

		return $shortcode;
	}
}

VOTING_shortcode::init();