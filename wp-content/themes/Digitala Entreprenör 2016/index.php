<?php get_header() ?>
<?php
if ( isset( $_POST['submit'] ) ) {
	var_dump( $_POST );
}
?>
<div class="container">
	<div class="modal" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">

			</div>
		</div>
	</div>
	<?php
	// WP_Query arguments
	$args = array(
		'post_type' => array( 'vote' ),
	);

	// The Query
	$vote = new WP_Query( $args );

	// The Loop
	if ( $vote->have_posts() ) : while ( $vote->have_posts() ) :
		$vote->the_post(); ?>
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="content">
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="thumbnail">
							<?php $image = get_the_post_thumbnail() ?>
							<?php echo $image ?>
						</div>
					<?php else : ?>
						<div class="default thumbnail">
							<img src="<?php echo get_bloginfo( 'template_url' ) . "/images/default.jpg" ?>"
							     alt="<?php _e( 'default image', 'voting' ) ?>">
						</div>
					<?php endif ?>
					<div class="caption">
						<h2 id="entrant"><?php esc_attr( the_title() ) ?></h2>
						<p><?php esc_attr( the_excerpt() ) ?></p>
						<a class="readmore btn btn-primary"
						   href="<?php esc_url( the_permalink() ) ?>"><?php _e( 'Read more', 'voting' ) ?></a>
						<?php do_shortcode('[vote]') ?>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile;
	else : ?>
		<div class="content">
			<h2 class="not-found">
				<?php _e( 'No attendant found', 'voting' ) ?>
			</h2>
		</div>
	<?php endif; ?>
</div>
<?php wp_reset_postdata(); ?>
<?php get_footer() ?>
