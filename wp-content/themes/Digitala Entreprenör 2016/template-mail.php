<?php
/*
 * Template Name: Mail
 */
?>
<?php get_header()?>
<div class="container">
	<?php
	$args = array(
		'post_type' => array('page'),
	);
	$query = new WP_Query( $args );
	?>
	<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
		<h1><?php the_title()?></h1>
	<?php endwhile; endif; ?>
	<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]')?>
</div>
<?php get_footer()?>