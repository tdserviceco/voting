+ function() {
    $( 'body > *:not(#site-footer, script)').wrapAll($('<div>', {'id' : 'wrap'}));
    $( '#wrap > *').wrapAll($('<div>', {'id' : 'main'}));
    $(window).on('load resize', function() {

        $('#main').css('padding-bottom', $('#site-footer').outerHeight() );
        $('#site-footer').css('margin-top', -parseInt($('#site-footer').outerHeight()));
    });
}()