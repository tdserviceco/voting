// Bower Components
@import "../../../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js";
@import "../../../bower_components/slick-carousel/slick/slick.js";
@import "../../../bower_components/jquery.syncHeight/jquery.syncheight.js";
@import "../../../bower_components/moment/min/moment.min.js";

// Local Parts
jQuery(document).ready(function($) {
    @import "parts/_footer.js";
});