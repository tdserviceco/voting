<?php get_header() ?>
    <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="content">
                <h2><?php the_title()?></h2>
            </div>
        <?php endwhile; endif ?>
    </div>
<?php get_footer() ?>