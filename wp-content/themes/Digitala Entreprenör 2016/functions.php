<?php

class DE2016 {
	static function init() {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'theme_support' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'load_text_domain' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'register_my_menu' ) );
	}

	static function register_my_menu() {
		register_nav_menu( 'primary', __( 'Primary Menu', 'voting' ) );
	}

	static function scripts() {
		wp_enqueue_script( 'app', get_bloginfo( "template_url" ) . '/assets/js/app.js', array( 'jquery' ), false, true );
		wp_enqueue_style( 'stylesheet', get_bloginfo( "template_url" ) . '/assets/css/app.css' );
	}

	static function theme_support() {
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'post-thumbnails' );
	}

	static function load_text_domain() {
		load_theme_textdomain( 'voting', TEMPLATEPATH.'/languages' );
	}

}

include __DIR__ . "/inc/wp_bootstrap_navwalker.php";
DE2016::init();